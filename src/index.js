import React from 'react';
import ReactDOM from 'react-dom';
import './theme/css/bootstrap.min.css';
import './theme/css/simple-sidebar.css';
import './css/custom.css';
import PageMain from './containers/PageMain';
import PageView from './containers/PageView';
import PageLogin from './containers/PageLogin';
import PageRegister from './containers/PageRegister';
import PageTutorial from './containers/PageTutorial';
import PageAbout from './containers/PageAbout';
import PageConnector from './containers/PageConnector';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, hashHistory } from 'react-router';
import reducer from './reducers'; //подцепит index.js

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
const history = syncHistoryWithStore(hashHistory, store);

ReactDOM.render(
	<Provider store={store}>
		<div>
            <Router history={history}>
                <Route path="/" hist={history} component={PageMain} />
                <Route path="/view/:id" component={PageView} />
                <Route path="/login" component={PageLogin} />
                <Route path="/signup" component={PageRegister} />
                <Route path="/tutorial" component={PageTutorial} />
                <Route path="/about" component={PageAbout} />
                <Route path="/connector" component={PageConnector} />
            </Router>
		</div>
	</Provider>,
	document.getElementById('root')
);



