import Menu from '../components/Menu';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PostItem from '../components/PostItem';
import { getDronList } from '../actions/dronActions';
import { checkAuth } from '../actions/loginAction';
import Header from '../components/Header';
import {Card, Row, Col } from 'reactstrap';
import Callback from '../components/Callback';



class PageMain extends Component {
    componentWillMount() {
        //Вообще-то так нельзя делать, должен быть отдельный метод который проверяет на авторизацию
        if(localStorage.authToken === undefined)
            this.props.route.hist.push('login');
        else
            this.props.onGetList(this.props.filter);
    }

    render() {
        const ad_posts = this.props.adData;
        return (
            <div>
                <Header />
                <div id="wrapper">
                    <div className="page-content-wrapper">
                        <div id="main">
                            <div className="container-fluid">
                                <Row>
                                    <Col lg="12">
                                        <Menu />
                                        <Row className="row">
                                            <Col md="12" className="content-page">
                                                {(()=> {
                                                    let list = ad_posts.map((item, key) => {
                                                        return <PostItem key={key} itemData={item} />
                                                    });
                                                    return list;
                                                })()}
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                </div>
                <Callback />
            </div>
        )
    }
}

export default connect(
  (state, ownProps) => ({
      ownProps,
      adData: state.dronsData.adData,
      userData: state.userData
  }),
  dispatch => ({
      onGetList: (filter) => {
          dispatch(getDronList(filter));
      }
  })
)(PageMain);