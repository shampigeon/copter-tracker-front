import React, { Component } from 'react';
import { connect } from 'react-redux';
import Menu from '../components/Menu';
import Header from '../components/Header';
import Connector from '../components/Connector';

class PageConnector extends Component {

    render() {
        return (
        <div>
            <Header />
            <div id="wrapper">
                <div className="page-content-wrapper">
                    <div id="main">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-lg-12">
                                    <Menu />
                                    <Connector />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps
    }),
    dispatch => ({
    })
)(PageConnector);