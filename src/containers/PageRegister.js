import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signupUser } from '../actions/loginAction';
import Header from '../components/Header';


class PageRegister extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            email: "",
            password: ""
        };

        this.usernameChange = this.usernameChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.passwdChange = this.passwdChange.bind(this);
        this.tryRegister = this.tryRegister.bind(this);
    }

    usernameChange(e) {
        this.setState({username: e.target.value})
    }
    emailChange(e) {
        this.setState({email: e.target.value})
    }

    passwdChange(e) {
        this.setState({password: e.target.value})
    }

    tryRegister() {

        this.props.onSendData(this.state);
    }
//     SignupForm[username]:admin222
//     SignupForm[email]:admin@admin.loc22
//      SignupForm[password]:йцуцукцук


    render() {

        return (
            <div>
                <Header />
                <div id="main">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12 ">
                                <div style={{width: "350px", margin: "0px auto", marginTop: '200px', background: "#fff", padding: "20px"}}>
                                    <div className="form-group">
                                        <h4>Регистрация</h4>
                                    </div>
                                    <hr />
                                    <div className="form-group">
                                        <label htmlFor="username">Имя пользователя</label>
                                        <input type="text" className="form-control" id="username" placeholder="Enter email"
                                               value={this.state.username} onChange={this.usernameChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input type="text" className="form-control" id="email" placeholder="Enter email"
                                               value={this.state.email} onChange={this.emailChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="passwd">Пароль</label>
                                        <input type="password" id="passwd" className="form-control"
                                               value={this.state.password} onChange={this.passwdChange}/>
                                    </div>
                                    <div className="form-group text-center">
                                        <button className="btn btn-success" onClick={this.tryRegister}>Войти</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps
    }),
    dispatch => ({
        onSendData: (value) => {
            dispatch(signupUser(value));
        },
    })
)(PageRegister);