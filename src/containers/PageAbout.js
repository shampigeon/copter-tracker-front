import React, { Component } from 'react';
import { connect } from 'react-redux';
import Menu from '../components/Menu';
import Header from '../components/Header';
import {Card, Row, Col } from 'reactstrap';
import Callback from '../components/Callback';

class PageAbout extends Component {

    render() {
        return (
            <div>
                <Header />
                <div id="wrapper">
                    <div className="page-content-wrapper">
                        <div id="main">
                            <div className="container-fluid">
                                <Row>
                                    <Col lg="12">
                                        <Menu />
                                        <Card>
                                            <div className="card-header"><b>О нас</b></div>
                                            <div className="card-block">
                                                <h2>О нас</h2>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat ante felis, non
                                                fringilla dolor euismod et. In aliquam diam nulla, ut eleifend quam malesuada ac. Curabitur
                                                ultrices lacus erat, quis mattis augue auctor vel. Aliquam erat volutpat. Suspendisse vitae
                                                enim consectetur, luctus erat id, efficitur risus. Suspendisse finibus justo a lectus tincidunt,
                                                at vulputate ligula vestibulum. Donec et feugiat justo, sed mollis quam. Ut facilisis est
                                                eget erat facilisis, in luctus diam cursus. Aliquam quis arcu lacinia, sagittis velit
                                                placerat, imperdiet ex. Quisque ut elit non eros ornare convallis a vel sem. Phasellus at
                                                maximus lorem.

                                                Integer pretium, tortor sed consectetur aliquet, magna diam hendrerit purus, ut sagittis
                                                ligula velit et mauris. Nunc viverra tellus sapien, sit amet convallis erat tempor eget.
                                                Pellentesque viverra nunc in tincidunt fringilla. Etiam consectetur sapien eget mi blandit
                                                condimentum. Nullam laoreet iaculis odio, et luctus erat auctor id. Nullam mattis enim vel
                                                sapien dapibus, ut sodales sem faucibus. Proin nec justo a mi pellentesque dignissim eget quis ante.

                                                Aliquam tincidunt placerat turpis convallis elementum. Suspendisse egestas sapien quam,
                                                vitae hendrerit felis fermentum ut. Phasellus elementum sed urna nec consectetur. Mauris in
                                                nunc interdum nibh dictum venenatis. Morbi pulvinar nunc ut mauris facilisis, et tristique
                                                neque venenatis. Nunc pulvinar maximus dictum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean blandit odio velit, vitae euismod enim sagittis ac. Donec augue metus, hendrerit quis maximus placerat, faucibus placerat tortor. Mauris dolor diam, mattis vestibulum mi quis, pulvinar auctor dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis in bibendum erat. Morbi non elit at justo rutrum cursus non id ligula. Nam sagittis elit massa. Quisque blandit risus suscipit dui pharetra varius. In nec tellus sed odio dictum pellentesque.

                                                Pellentesque ut lorem vitae massa fermentum molestie. Fusce sed eros eget libero sagittis
                                                molestie tempus nec nunc. Vivamus eget feugiat nisi, in fermentum est. Proin ac augue nec leo
                                                blandit dapibus sed eu nibh. Sed convallis vehicula aliquam. Vivamus faucibus mollis ante
                                                at scelerisque. Vestibulum condimentum accumsan augue id sollicitudin. Quisque quis mauris mollis,
                                                luctus eros efficitur, dapibus leo.

                                                Donec eget consequat odio, a mollis mauris. Donec blandit quam at magna convallis venenatis.
                                                Nulla posuere imperdiet ligula, aliquet dignissim massa condimentum sit amet. Donec non elementum
                                                velit. Mauris et dignissim orci, sit amet dapibus ligula. Etiam tempus mauris eros, nec tristique
                                                odio aliquam quis. Etiam rhoncus gravida finibus.
                                            </div>
                                        </Card>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                </div>
                <Callback/>
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps
    }),
    dispatch => ({
    })
)(PageAbout);