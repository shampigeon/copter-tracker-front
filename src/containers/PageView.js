import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDronInfo } from '../actions/dronActions';
import { YMaps, Map, Placemark, Marker } from 'react-yandex-maps';
import Menu from '../components/Menu';
import Header from '../components/Header';
import {Card, Row, Col } from 'reactstrap';


class PageView extends Component {

    componentWillMount() {
        //Вообще-то так нельзя делать, должен быть отдельный метод который проверяет на авторизацию
        if(localStorage.authToken === undefined)
            this.props.route.hist.push('login');
        else
            this.props.onGetDronInfo(this.props.ownProps.params.id);
    }

    render() {
        // Вообще этого здесь не должно быть
        const mapState = {
            center: [55.76, 37.64],
            zoom: 13
        };
        const dronInfo = this.props.dronInfo;
        return (
        <div>
            <Header />
            <div id="wrapper">
                <div className="page-content-wrapper">
                    <div id="main">
                        <div className="container-fluid">
                            <Row>
                                <Col lg="12">
                                    <Menu />
                                    <Card>
                                        <div className="card-header"><b>{dronInfo.title} информация</b></div>
                                        <div className="card-block">
                                            <div className="dron-inf">
                                                <div className="dron-inf__imgwrap">
                                                    <img width="350" src={dronInfo.image} />
                                                </div>
                                                <h3>Подробная информация</h3>
                                                {dronInfo.text}
                                            </div>
                                            <hr />
                                            <div style={{textAlign: 'center'}}><h4>Местоположение в данный момент</h4></div>
                                            <div>
                                                <YMaps>
                                                    <Map width='100%'
                                                         height={500}
                                                         state={mapState}>
                                                        <Placemark
                                                            geometry={{
                                                                coordinates: dronInfo.coordinates // [55.751574, 37.573856]
                                                            }}
                                                            properties={{
                                                                hintContent: 'Местоположение дрона',
                                                                balloonContent: 'Дрон здесь'
                                                            }}
                                                            options={{
                                                                iconLayout: 'default#image',
                                                                iconImageSize: [30, 42],
                                                                iconImageOffset: [-3, -42]
                                                            }}
                                                        />
                                                    </Map>
                                                </YMaps>
                                            </div>
                                        </div>
                                    </Card>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        dronInfo: state.dronsData.dronInfo,
        ownProps  //передаем данные о роутинге (location, query и прочее)
    }),
    dispatch => ({
        onGetDronInfo: (id) => {
            dispatch(getDronInfo(id));
        },
    })
)(PageView);