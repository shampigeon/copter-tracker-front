import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginUser } from '../actions/loginAction';
import Header from '../components/Header';


class PageLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: "",
            passwd: ""
        };

        this.loginChange = this.loginChange.bind(this);
        this.passwdChange = this.passwdChange.bind(this);
        this.tryLogin = this.tryLogin.bind(this);
    }

    loginChange(e) {
        this.setState({login: e.target.value})
    }

    passwdChange(e) {
        this.setState({passwd: e.target.value})
    }

    tryLogin() {
        if(this.state.login.length > 0 && this.state.passwd.length > 0)
            this.props.onSendData({
                login: this.state.login,
                passwd: this.state.passwd
            });
    }

    render() {
        return (
            <div id="main">
                <Header/>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div style={{width: "350px", margin: "0px auto", marginTop: '200px', background: "#fff", padding: "20px"}}>
                                <div className="form-group">
                                    <h4>Вход</h4>
                                </div>
                                <hr />
                                <div className="form-group">
                                    <span className="badge badge-danger">{this.props.userData.message}</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="login">Логин</label>
                                    <input type="email" className="form-control" placeholder="Enter email"
                                           value={this.state.login} onChange={this.loginChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="login">Пароль</label>
                                    <input type="password" className="form-control"
                                           value={this.state.passwd} onChange={this.passwdChange}/>
                                </div>
                                <div className="form-group text-center">
                                    <button className="btn btn-success" onClick={this.tryLogin}>Войти</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps,
        userData: state.userData
    }),
    dispatch => ({
        onSendData: (value) => {
            dispatch(loginUser(value));
        },
    })
)(PageLogin);