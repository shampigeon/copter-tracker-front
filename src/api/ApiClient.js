//TODO:: Реализовать нормальный клиент
import {hashHistory } from 'react-router';


export default class ApiClient {
    constructor({ prefix = '' } = {}) {
        this.prefix = prefix;
    }


    get(requestUrl, params = {}, callback) {
        return this.request(
            requestUrl,
            'get',
            params,
            callback
        );
    }

    post(requestUrl, params = {}, callback) {
        return this.request(
            requestUrl,
            'post',
            params,
            callback
        );
    }

    request(url, method, params = {}, firstCall) {
        let headers = new Headers();
        // headers.append('Accept', 'application/json');
        // headers.append('Content-Type', 'application/json');

        let options = {
            credentials: "same-origin",
            method: method,
            headers: headers,
            mode: "cors",
        };

        if(method !== 'get' && method !== 'head') {
            let data = new FormData();
            for(var key in params) {
                data.append(key, params[key]);
            }
            options.body = data;
        }

        return fetch(url, options).then((response) => {
            if (response.status == 401) {
                hashHistory.push('/login'); //Этого здесь не должно быть
                // throw new Error('Not authorized');
            }
            if (response.status == 400 || response.status > 401) {
                throw new Error('Bad response from server');
            }
            return response.json();
        }).then(firstCall);
    }

}