import { NOT_AUTH } from '../reducers/constant';
import {hashHistory } from 'react-router';
import CONFIG from '../configs/mainconf';
import ApiClient from '../api/ApiClient';
const Api = new ApiClient();
const domain = CONFIG.domain;

export const loginUser = (value = {}) => {
    if(value.login && value.passwd) {
        let opts = {
            'LoginForm[email]': value.login,
            'LoginForm[password]': value.passwd
        };

        return dispatch => {
            Api.post(
                domain + '/site/login',
                opts,
                (data) => {
                    if(data.success && data.token){
                        localStorage.setItem('authToken', data.token)
                        hashHistory.push('/'); //Этого здесь не должно быть
                    } else {
                        dispatch({type: NOT_AUTH, payload: data.message});
                    }
                }
            );
        }
    }
};

export const signupUser = (params = {}) => {
    if(Object.keys(params).length > 0) {
        let opts = {
            'SignupForm[username]': params.username,
            'SignupForm[email]': params.email,
            'SignupForm[password]': params.password
        };

        return dispatch => {
            Api.post(
                domain + '/site/signup',
                opts,
                (data) => {
                    if(data.success && data.token){
                        localStorage.setItem('authToken', data.token)
                        hashHistory.push('/'); //Этого здесь не должно быть
                    } else {
                        dispatch({type: NOT_AUTH, payload: data.message});
                    }
                }
            );
        }
    }
};