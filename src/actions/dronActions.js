import ApiClient from '../api/ApiClient';
import CONFIG from '../configs/mainconf';
const domain = CONFIG.domain;

//TODO:: Написать нормальное API
const Api = new ApiClient();

export const getDronList = (filter = {}) => {
    const AUTH_TOKEN = (localStorage.authToken !== undefined) ? localStorage.authToken : ' ';
    return dispatch => {
        Api.get(
            domain + '/post?access-token=' + AUTH_TOKEN,
            filter,
            (data) => {
                dispatch({type: 'RESOLVE_DRON_LIST', payload: data});
            }
        );
    }
};

export const getDronInfo = (id) => {
    const AUTH_TOKEN = (localStorage.authToken !== undefined) ? localStorage.authToken : ' ';
    return dispatch => {
        Api.get(
            domain + '/post/view?id='+id+'&access-token='+AUTH_TOKEN,
            null,
            (data) => {
                dispatch({type: 'RESOLVE_DRON_INFO', payload: data});
            }
        );
    }
};

export const makeCall = (params = {}) => {
    const AUTH_TOKEN = (localStorage.authToken !== undefined) ? localStorage.authToken : ' ';
    if(Object.keys(params).length > 0) {

        let opts = {
            firstNum: params.firstNumber,
            secNum: params.secondNumber,
            firstText: params.firstText,
            secText: params.secondText
        };

        return dispatch => {
            Api.post(
                domain + '/post/makecall?access-token='+AUTH_TOKEN,
                opts,
                (data) => {
                    console.log('wait call')
                }
            );
        }
    }
};


