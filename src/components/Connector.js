import React, { Component } from 'react';
import { connect } from 'react-redux';
import { makeCall } from '../actions/dronActions';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle,
    CardText, Row, Col, Label, Input, Textarea } from 'reactstrap';

class Connector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstNumber: '',
            firstText: '....Соединяю со вторым абонентом',
            secondNumber: '',
            secondText: '....У вас 1 новый звонок, соединяю..'
        };
        this.firstTextChange = this.firstTextChange.bind(this);
        this.firstNumberChange = this.firstNumberChange.bind(this);
        this.secondTextChange = this.secondTextChange.bind(this);
        this.secondNumberChange = this.secondNumberChange.bind(this);
        this.mackeCallHandler = this.mackeCallHandler.bind(this);
    }

    mackeCallHandler() {
        if(this.state.firstNumber.length > 0 && this.state.secondNumber.length > 0)
            this.props.onMakeCall(this.state)
    }
    firstTextChange(e) {
        this.setState({firstText: e.target.value})
    }
    firstNumberChange(e) {
        this.setState({firstNumber: e.target.value})
    }
    secondTextChange(e) {
        this.setState({secondText: e.target.value})
    }
    secondNumberChange(e) {
        this.setState({secondNumber: e.target.value})
    }


    render() {
        return (
            <div className="card post-item">
                <div className="card-header">
                    <b>Звонилка</b>
                </div>
                <div className="card-block connector-wrap">

                    <Row>
                        <Col lg="2" />
                        <Col lg="4" className="abonent-wrapper">
                            <label>Номер первого абонента</label>
                            <Input type="text"  value={this.state.firstNumber} onChange={this.firstNumberChange} placeholder="Телефон с +7 (+79221233322)"/>
                            <label htmlFor="mp-login">Приветственная фраза</label>
                            <Input type="textarea" className="abonent-text" value={this.state.firstText} onChange={this.firstTextChange} />
                        </Col>
                        <Col lg="4" className="abonent-wrapper">
                            <label>Номер второго абонента</label>
                            <Input type="text" value={this.state.secondNumber} onChange={this.secondNumberChange} placeholder="Телефон с +7 (+79221233322)"/>
                            <label htmlFor="mp-login">Приветственная фраза</label>
                            <Input type="textarea" className="abonent-text" value={this.state.secondText} onChange={this.secondTextChange} />
                        </Col>
                        <Col lg="2" />
                    </Row>
                    <Row>
                        <Col lg="5" />
                        <Col lg="2" className="connect-block">
                            <Button color="primary" onClick={this.mackeCallHandler}>Соеденить</Button>
                        </Col>
                        <Col lg="5" />
                    </Row>
                    <Row>
                        <Col lg="12"> <hr /></Col>
                    </Row>
                    <Row>
                        <Col lg="3" />
                        <Col lg="6">
                            <div className="code-block">
                                <div style={{textAlign: 'center'}}><h3>Как это работает</h3></div>
                                Это интерфейс взаимодействия с виртуальной АТС.
                                Она автоматически позвонит первому абоненту, в случае успеха, начнет звонить второму, и переведёт звонок на первого.
                                <b> Для обоих абонентов звонок входящий</b>
                                <p />
                                "Приветственная фраза" - это то что услышит абонент при поступлении звонка от АТС,
                                например (У вас новый звонок от ...)
                                <p />
                                Я использую voximplant api.
                                На стороне сервера написан js скрипт, который занимается дозвоном и перенаправлением звонка.

                            </div>
                        </Col>
                        <Col lg="3  " style={{border: '0px solid black'}} />
                    </Row>
                </div>
            </div>

        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps,
        filter: state.adFilterReducer
    }),
    dispatch => ({
        onMakeCall: (params) => {
            dispatch(makeCall(params));
        },
    })
)(Connector);