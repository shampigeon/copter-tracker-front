import React, { Component } from 'react';
import { Link } from 'react-router';
import {hashHistory } from 'react-router';
import CONFIG from '../configs/mainconf';


class Header extends Component {

    logoutHandler() {
        localStorage.clear();
        hashHistory.push('login');
    }

  render() {
    return (
        <div className="header">
            <div className="header__container_left">
                <Link to='/'><img  height="60" src={CONFIG.domain +"/img/ke1.svg"} /></Link>
                <div>Панель&nbsp;управления</div>
            </div>
            <div className="header__container_right">
                <div className="header__user-info_name">
                    {(()=> {
                        if(localStorage.authToken !== undefined)
                            return <a href="#" onClick={this.logoutHandler}>Выход</a>
                        else {
                            return <span>
                                        <Link key="1" to='/login'>Вход</Link>&nbsp;
                                        <Link key="2" to='/signup'>Регистрация</Link>
                                </span>
                        }
                    })()}

                </div>
            </div>
        </div>
    )
  }
}

export default Header;