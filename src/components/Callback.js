import React, { Component } from 'react';
import { makeCall } from '../actions/dronActions';
import CONFIG from '../configs/mainconf';
import { Input, Button } from 'reactstrap';
import { connect } from 'react-redux';

class Callback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            number: '',
            show: false
        };

        this.numberChangeHandler = this.numberChangeHandler.bind(this);
        this.showHandler = this.showHandler.bind(this);
        this.closeHandler = this.closeHandler.bind(this);
        this.callHandler = this.callHandler.bind(this);
    }

    numberChangeHandler(e) {
        this.setState({number: e.target.value})
    }

    showHandler(e) {
        this.setState({show: true});
    }

    closeHandler() {
        this.setState({show: false});
    }

    callHandler() {
        let params = {
            firstNumber: this.state.number,
            firstText: '....Спасибо, соединяю...',
            secondNumber: '+79112673337',
            secondText: '....У вас 1 новый звонок, соединяю..'
        };

        this.props.onMakeCall(params);
        this.setState({show: false});
    }

    render() {
        return (
            <div>
                <div className={(this.state.show) ? "modal-callback" : "modal-callback hide"}>
                    <div className="modal-callback__head">
                        <span onClick={this.closeHandler}>&#10006;</span>
                    </div>
                    <div className="modal-callback__text">
                        Оставьте свой номер и я перезвоню Вам в течение 30 секунд
                    </div>
                    <div className="modal-callback__input">
                        <Input type="text" onChange={this.numberChangeHandler} placeholder="+7(xxx)-xxx-xx-xx" value={this.state.number}/>
                    </div>
                    <div className="text-center">
                        <Button color="primary" onClick={this.callHandler}>Жду звонка</Button>
                    </div>
                </div>
                <div className={(this.state.show) ? "black-overlay" : "black-overlay hide"}></div>

                <img className={(this.state.show) ? "callimg hide" : "callimg"} style={{width: '130px'}} onClick={this.showHandler} src={CONFIG.domain + '/img/call1.gif'} />
            </div>
        )
    }
}

export default connect(
    (state, ownProps) => ({
        ownProps,
    }),
    dispatch => ({
        onMakeCall: (params) => {
            dispatch(makeCall(params));
        },
    })
)(Callback);