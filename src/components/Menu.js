import React, { Component } from 'react';
import { Link } from 'react-router';

class Menu extends Component {

  render() {
    return (
        <div id="sidebar-wrapper" className="main-menu">
            <ul className="sidebar-nav">
                <li>
                    <Link to='/'>Список дронов</Link>
                </li>
                <li>
                    <Link to='/tutorial'>Справка</Link>
                </li>
                <li>
                    <Link to='/about'>О нас</Link>
                </li>
                <li>
                    <Link to='/connector'>Звонилка&nbsp;<span style={{width: '50px', color: 'red', fontWeight: 'bold'}}>new</span></Link>
                </li>

            </ul>
        </div>
    )
  }
}

export default Menu;