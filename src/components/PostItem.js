import React, { Component } from 'react';
import { Link } from 'react-router';


class PostItem extends Component {

    render() {
        let info = this.props.itemData;

        return (
          <div className="card post-item">
              <div className="card-header">
                  <span><Link to={`/view/${info.id}`}>{info.title}</Link></span>
                  <span className="post-item__date" title="Дата последнего обновления">
                      <Link to={`/view/${info.id}`} className="btn btn-sm btn-info">Подробнее</Link>
                  </span>
              </div>
              <div className="card-block">
                  <div className="post-item__imgwrap">
                    <img src={info.image} />
                  </div>
                  <div className="post-item__info">
                      <div className="post-item__content">
                          <table className="table table-outline">
                              <thead>
                                  <tr>
                                      <th>Задача</th>
                                      <th>Клиент</th>
                                      <th>Стутус</th>
                                      <th>% заряда</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td>{info.info}</td>
                                    <td>{info.client}</td>
                                    <td>{info.status}</td>
                                    <td>{info.battery} &nbsp;{(()=> {
                                        if(Number(info.battery < 15))
                                            return <span className="badge badge-danger">Низкий заряд</span>
                                    })()}</td>
                                </tr>
                                <tr>
                                    <th>Заголовок 1</th>
                                    <th>Заголовок 2</th>
                                    <th>Заголовок 3</th>
                                    <th>Заголовок 4</th>

                                </tr>
                                <tr>
                                    <td>lorem ipsum</td>
                                    <td>sit amit</td>
                                    <td> Nam volutpat ante felis</td>
                                    <td>non fringilla dolor</td>
                                </tr>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

export default PostItem;