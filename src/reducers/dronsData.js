import {RESOLVE_DRON_LIST, RESOLVE_DRON_INFO} from './constant'

const initialState = {
    adData: [],
    dronInfo: []
};

export default function advertsData(state = initialState, action) {
    if(action.type === RESOLVE_DRON_LIST) {
        return {
            ...state,
            adData: action.payload
        }
    } else if(action.type === RESOLVE_DRON_INFO) {
        return {
            ...state,
            dronInfo: action.payload
        }
    }
    return state;
}