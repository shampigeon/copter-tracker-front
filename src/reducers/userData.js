import { AUTH_SUCCESS, NOT_AUTH } from './constant';

const initialState = {
    name: "",
    message: "",
    role: "",
};

export default function userData(state = initialState, action) {
    if(action.type === AUTH_SUCCESS) {
        return {
            ...state,
            authorized: action.payload
        }
    } else if(action.type === NOT_AUTH) {
        return {
            ...state,
            message: action.payload
        }
    }
    return state;
}