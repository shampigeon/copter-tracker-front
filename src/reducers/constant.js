export const RESOLVE_DRON_LIST = 'RESOLVE_DRON_LIST';
export const RESOLVE_DRON_INFO = 'RESOLVE_DRON_INFO';
export const NOT_AUTH = 'NOT_AUTH';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';