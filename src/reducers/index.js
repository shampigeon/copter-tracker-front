import { combineReducers } from 'redux';
import dronsData from './dronsData';
import userData from './userData';

import { routerReducer } from 'react-router-redux';

export default combineReducers({
	routing: routerReducer,
	dronsData,
	userData
})